const express = require('express');
const axios = require('axios');
const cors = require('cors');

const port = 3000;
const app = express();

app.use('*', cors());

const username = '';

app.get('/', (req, res) => {
    let q = req.query.q;
    axios.get(`https://api.company-information.service.gov.uk/search?q=${q}`,
        {auth: { username, password: '' }, withCredentials: true})
        .then(response => res.json(response.data));

});

app.listen(port, () => {
    console.log(`Translation API Proxy listening at http://localhost:${port}`)
});
